program Painter;

uses
  Forms,
  uMain in 'uMain.pas' {FormMain},
  uPoint in 'uPoint.pas' {FormPoint},
  uAbout in 'uAbout.pas' {FormAbout},
  uColor in 'uColor.pas' {FormColor};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'H.B. Paint';
  Application.CreateForm(TFormMain, FormMain);
  Application.CreateForm(TFormPoint, FormPoint);
  Application.CreateForm(TFormAbout, FormAbout);
  Application.CreateForm(TFormColor, FormColor);
  Application.Run;
end.
