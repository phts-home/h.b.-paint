unit uAbout;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, FlatButton, StdCtrls, ShellApi;

type
  TFormAbout = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    FlatButton2: TFlatButton;
    Image1: TImage;
    procedure FlatButton2Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure Label6Click(Sender: TObject);
    procedure Label7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormAbout: TFormAbout;

implementation

uses uMain;

{$R *.dfm}

procedure TFormAbout.FlatButton2Click(Sender: TObject);
begin
Close;
end;

procedure TFormAbout.FormKeyPress(Sender: TObject; var Key: Char);
begin
if (Key=#27)or(Key=#13) then Close;
end;

procedure TFormAbout.FormShow(Sender: TObject);
begin
FormAbout.Left:=FormMain.Left+65;
FormAbout.Top:=FormMain.Top+90;
end;

procedure TFormAbout.Label6Click(Sender: TObject);
begin
ShellExecute(0,'',pchar('mailto:'+Label6.Caption+'?subject=H.B. Paint'),'','',1);
end;

procedure TFormAbout.Label7Click(Sender: TObject);
begin
ShellExecute(0,'',pchar(Label7.Caption),'','',1);
end;

end.
 