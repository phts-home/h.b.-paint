object FormColor: TFormColor
  Left = 304
  Top = 222
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1094#1074#1077#1090#1086#1074
  ClientHeight = 343
  ClientWidth = 301
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 20
    Top = 15
    Width = 106
    Height = 21
  end
  object Image2: TImage
    Left = 20
    Top = 45
    Width = 105
    Height = 21
  end
  object Image3: TImage
    Left = 20
    Top = 75
    Width = 105
    Height = 21
  end
  object Image4: TImage
    Left = 20
    Top = 105
    Width = 105
    Height = 21
  end
  object Image5: TImage
    Left = 20
    Top = 135
    Width = 105
    Height = 56
  end
  object Image6: TImage
    Left = 20
    Top = 200
    Width = 105
    Height = 56
  end
  object Image7: TImage
    Left = 20
    Top = 265
    Width = 106
    Height = 21
  end
  object Bevel1: TBevel
    Left = 20
    Top = 15
    Width = 106
    Height = 21
    Style = bsRaised
  end
  object Bevel2: TBevel
    Left = 20
    Top = 45
    Width = 106
    Height = 21
    Style = bsRaised
  end
  object Bevel3: TBevel
    Left = 20
    Top = 75
    Width = 106
    Height = 21
    Style = bsRaised
  end
  object Bevel4: TBevel
    Left = 20
    Top = 105
    Width = 106
    Height = 21
    Style = bsRaised
  end
  object Bevel5: TBevel
    Left = 20
    Top = 265
    Width = 106
    Height = 21
    Style = bsRaised
  end
  object Bevel6: TBevel
    Left = 20
    Top = 135
    Width = 106
    Height = 56
    Style = bsRaised
  end
  object Bevel7: TBevel
    Left = 20
    Top = 200
    Width = 106
    Height = 56
    Style = bsRaised
  end
  object ColorBox1: TColorBox
    Left = 140
    Top = 15
    Width = 145
    Height = 22
    BevelKind = bkFlat
    BevelOuter = bvNone
    ItemHeight = 16
    TabOrder = 0
    OnChange = ColorBox1Change
  end
  object ColorBox2: TColorBox
    Left = 140
    Top = 45
    Width = 145
    Height = 22
    BevelKind = bkFlat
    BevelOuter = bvNone
    ItemHeight = 16
    TabOrder = 1
    OnChange = ColorBox2Change
  end
  object ColorBox3: TColorBox
    Left = 140
    Top = 75
    Width = 145
    Height = 22
    BevelKind = bkFlat
    BevelOuter = bvNone
    ItemHeight = 16
    TabOrder = 2
    OnChange = ColorBox3Change
  end
  object ColorBox4: TColorBox
    Left = 140
    Top = 105
    Width = 145
    Height = 22
    BevelKind = bkFlat
    BevelOuter = bvNone
    ItemHeight = 16
    TabOrder = 3
    OnChange = ColorBox4Change
  end
  object ColorBox5: TColorBox
    Left = 140
    Top = 135
    Width = 145
    Height = 22
    BevelKind = bkFlat
    BevelOuter = bvNone
    ItemHeight = 16
    TabOrder = 4
    OnChange = ColorBox5Change
  end
  object FlatButton2: TFlatButton
    Left = 210
    Top = 305
    Width = 75
    Height = 25
    BevelOuter = bvNone
    Caption = 'Ok'
    Color = 7851263
    Ctl3D = True
    ParentCtl3D = False
    TabOrder = 5
    OnClick = FlatButton2Click
    ColorNormalState = 7851263
    ButtonType = Flat
    ColorHighLightEnable = True
  end
  object ColorBox6: TColorBox
    Left = 140
    Top = 200
    Width = 145
    Height = 22
    BevelKind = bkFlat
    BevelOuter = bvNone
    ItemHeight = 16
    TabOrder = 6
    OnChange = ColorBox6Change
  end
  object ColorBox7: TColorBox
    Left = 140
    Top = 265
    Width = 145
    Height = 22
    BevelKind = bkFlat
    BevelOuter = bvNone
    Ctl3D = True
    ItemHeight = 16
    ParentCtl3D = False
    TabOrder = 7
    OnChange = ColorBox7Change
  end
end
