unit uColor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, FlatButton;

type
  TFormColor = class(TForm)
    ColorBox1: TColorBox;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    ColorBox2: TColorBox;
    ColorBox3: TColorBox;
    Image4: TImage;
    ColorBox4: TColorBox;
    Image5: TImage;
    ColorBox5: TColorBox;
    FlatButton2: TFlatButton;
    Image6: TImage;
    ColorBox6: TColorBox;
    ColorBox7: TColorBox;
    Image7: TImage;
    Bevel1: TBevel;
    Bevel2: TBevel;
    Bevel3: TBevel;
    Bevel4: TBevel;
    Bevel5: TBevel;
    Bevel6: TBevel;
    Bevel7: TBevel;
    procedure ColorBox1Change(Sender: TObject);
    procedure ColorBox2Change(Sender: TObject);
    procedure ColorBox3Change(Sender: TObject);
    procedure ColorBox4Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ColorBox5Change(Sender: TObject);
    procedure FlatButton2Click(Sender: TObject);
    procedure ColorBox6Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ColorBox7Change(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    procedure DrawImage1;
    procedure DrawImage2;
    procedure DrawImage3;
    procedure DrawImage4;
    procedure DrawImage5;
    procedure DrawImage6;
    procedure DrawImage7;
    { Public declarations }
  end;

var
  FormColor: TFormColor;

implementation

uses uMain;

{$R *.dfm}

procedure TFormColor.ColorBox1Change(Sender: TObject);
begin
FormMain.ColorPoint:=ColorBox1.Selected;
DrawImage1;
end;

procedure TFormColor.ColorBox2Change(Sender: TObject);
begin
FormMain.ColorLine:=ColorBox2.Selected;
DrawImage2;
end;

procedure TFormColor.ColorBox3Change(Sender: TObject);
begin
FormMain.ColorAxis:=ColorBox3.Selected;
DrawImage3;
end;

procedure TFormColor.ColorBox4Change(Sender: TObject);
begin
FormMain.ColorLength:=ColorBox4.Selected;
DrawImage4;
end;

procedure TFormColor.ColorBox5Change(Sender: TObject);
begin
FormMain.ColorDelta:=ColorBox5.Selected;
DrawImage5;
end;

procedure TFormColor.ColorBox6Change(Sender: TObject);
begin
FormMain.ColorBind:=ColorBox6.Selected;
DrawImage6;
end;

procedure TFormColor.ColorBox7Change(Sender: TObject);
begin
FormMain.ColorBackgr:=ColorBox7.Selected;
DrawImage7;
end;

procedure TFormColor.FormCreate(Sender: TObject);
begin
ColorBox1.Selected:=FormMain.ColorPoint;
ColorBox2.Selected:=FormMain.ColorLine;
ColorBox3.Selected:=FormMain.ColorAxis;
ColorBox4.Selected:=FormMain.ColorLength;
ColorBox5.Selected:=FormMain.ColorDelta;
ColorBox6.Selected:=FormMain.ColorBind;
ColorBox7.Selected:=FormMain.ColorBackgr
end;

procedure TFormColor.FormShow(Sender: TObject);
begin
DrawImage1;
DrawImage2;
DrawImage3;
DrawImage4;
DrawImage5;
DrawImage6;
DrawImage7;
FormColor.Left:=FormMain.Left+65;
FormColor.Top:=FormMain.Top+70;
end;

procedure TFormColor.FlatButton2Click(Sender: TObject);
begin
Close;
end;

procedure TFormColor.DrawImage1;
begin
Image1.Canvas.Font:=FormMain.Image1.Canvas.Font;
Image1.Canvas.Pen.Color:=FormMain.ColorPoint;
Image1.Canvas.Pen.Width:=2;
Image1.Canvas.Ellipse(20,7,26,13);
Image1.Canvas.TextOut(2,1,'A1');
end;

procedure TFormColor.DrawImage2;
begin
Image2.Canvas.Font:=FormMain.Image1.Canvas.Font;
Image2.Canvas.Pen.Color:=FormMain.ColorLine;
Image2.Canvas.Pen.Width:=2;
Image2.Canvas.MoveTo(2,5);
Image2.Canvas.LineTo(100,17);
end;

procedure TFormColor.DrawImage3;
begin
Image3.Canvas.Font:=FormMain.Image1.Canvas.Font;
Image3.Canvas.Pen.Color:=FormMain.ColorAxis;
Image3.Canvas.Pen.Width:=3;
Image3.Canvas.MoveTo(2,17);
Image3.Canvas.LineTo(100,17);
Image3.Canvas.TextOut(10,0,'X');
end;

procedure TFormColor.DrawImage4;
begin
Image4.Canvas.Font:=FormMain.Image1.Canvas.Font;
Image4.Canvas.Pen.Color:=FormMain.ColorLength;
Image4.Canvas.Pen.Width:=3;
Image4.Canvas.MoveTo(2,5);
Image4.Canvas.LineTo(100,17);
Image4.Canvas.TextOut(50,1,'�.�.');
end;

procedure TFormColor.DrawImage5;
begin
Image5.Canvas.Font:=FormMain.Image1.Canvas.Font;
Image5.Canvas.Pen.Color:=FormMain.ColorDelta;
Image5.Canvas.Pen.Width:=3;
Image5.Canvas.MoveTo(10,5);
Image5.Canvas.LineTo(10,50);
Image5.Canvas.TextOut(15,25,'Delta Z');
end;

procedure TFormColor.DrawImage6;
begin
Image6.Canvas.Font:=FormMain.Image1.Canvas.Font;
Image6.Canvas.Pen.Color:=FormMain.ColorPoint;
Image6.Canvas.Pen.Width:=2;
Image6.Canvas.Ellipse(20,12,26,18);
Image6.Canvas.TextOut(2,1,'A2');
Image6.Canvas.Ellipse(20,40,26,46);
Image6.Canvas.TextOut(2,30,'A1');

Image6.Canvas.Pen.Color:=FormMain.ColorBind;
Image6.Canvas.Pen.Width:=1;
Image6.Canvas.MoveTo(23,5);
Image6.Canvas.LineTo(23,50);
end;

procedure TFormColor.DrawImage7;
begin
Image7.Canvas.Pen.Width:=1;
Image7.Canvas.Pen.Color:=clBlack;
Image7.Canvas.Brush.Color:=FormMain.ColorBackgr;
Image7.Canvas.Rectangle(0,0,Image7.Width,Image7.Height);
end;

procedure TFormColor.FormKeyPress(Sender: TObject; var Key: Char);
begin
if Key=#27 then Close;
end;

end.
