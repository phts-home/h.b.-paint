object FormMain: TFormMain
  Left = 240
  Top = 125
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'H.B. Paint'
  ClientHeight = 430
  ClientWidth = 557
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 5
    Top = 5
    Width = 544
    Height = 400
    Cursor = crCross
    OnMouseDown = Image1MouseDown
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 411
    Width = 557
    Height = 19
    Panels = <
      item
        Width = 250
      end
      item
        Alignment = taCenter
        Bevel = pbNone
        Text = 'A'
        Width = 20
      end
      item
        Text = #1053#1077' '#1080#1079#1074#1077#1089#1090#1085#1086
        Width = 75
      end
      item
        Alignment = taCenter
        Bevel = pbNone
        Text = 'B'
        Width = 20
      end
      item
        Text = #1053#1077' '#1080#1079#1074#1077#1089#1090#1085#1086
        Width = 75
      end
      item
        Alignment = taRightJustify
        Bevel = pbNone
        Text = #1085'.'#1074'. ='
        Width = 40
      end
      item
        Text = #1053#1077' '#1080#1079#1074#1077#1089#1090#1085#1086
        Width = 50
      end>
    SizeGrip = False
  end
  object MainMenu1: TMainMenu
    Left = 10
    Top = 10
    object N1: TMenuItem
      Caption = #1060#1072#1081#1083
      object N9: TMenuItem
        Caption = #1057#1086#1079#1076#1072#1090#1100
        ShortCut = 16462
        OnClick = N9Click
      end
      object N8: TMenuItem
        Caption = #1054#1090#1082#1088#1099#1090#1100'...'
        ShortCut = 16463
        OnClick = N8Click
      end
      object N14: TMenuItem
        Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1082#1072#1082'...'
        ShortCut = 16467
        OnClick = N14Click
      end
      object N15: TMenuItem
        Caption = '-'
      end
      object N4: TMenuItem
        Caption = #1042#1099#1093#1086#1076
        ShortCut = 16465
        OnClick = N4Click
      end
    end
    object N11: TMenuItem
      Caption = #1055#1088#1086#1075#1088#1072#1084#1084#1072
      object N16: TMenuItem
        Caption = #1062#1074#1077#1090#1072'...'
        OnClick = N16Click
      end
      object N12: TMenuItem
        Caption = #1064#1088#1080#1092#1090'...'
        OnClick = N12Click
      end
    end
    object N5: TMenuItem
      Caption = #1060#1091#1085#1082#1094#1080#1080
      object N10: TMenuItem
        Caption = #1055#1086#1089#1090#1088#1086#1080#1090#1100
        ShortCut = 13
        OnClick = N10Click
      end
      object N13: TMenuItem
        Caption = '-'
      end
      object N6: TMenuItem
        Caption = #1058#1086#1095#1082#1072' '#1040'...'
        ShortCut = 116
        OnClick = N6Click
      end
      object N7: TMenuItem
        Caption = #1058#1086#1095#1082#1072' '#1042'...'
        ShortCut = 117
        OnClick = N7Click
      end
    end
    object N2: TMenuItem
      Caption = #1057#1087#1088#1072#1074#1082#1072
      object N17: TMenuItem
        Caption = #1043#1086#1088#1103#1095#1080#1077' '#1082#1083#1072#1074#1080#1096#1080
        OnClick = N17Click
      end
      object N18: TMenuItem
        Caption = '-'
      end
      object N3: TMenuItem
        Caption = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077'...'
        OnClick = N3Click
      end
    end
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'bmp'
    Filter = 'BMP (*.bmp)|*.bmp'
    Left = 40
    Top = 10
  end
  object OpenDialog1: TOpenDialog
    Filter = 'BMP (*.bmp)|*.bmp'
    Left = 70
    Top = 10
  end
  object FontDialog1: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Left = 100
    Top = 10
  end
end
