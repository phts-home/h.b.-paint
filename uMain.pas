unit uMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Menus, ComCtrls, IniFiles;

type
  TMyPoint = Array [1..2] of Integer;
  TCoords = Array [1..3] of Integer;


type
  TFormMain = class(TForm)
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    Image1: TImage;
    N13: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    StatusBar1: TStatusBar;
    SaveDialog1: TSaveDialog;
    OpenDialog1: TOpenDialog;
    N8: TMenuItem;
    FontDialog1: TFontDialog;
    N11: TMenuItem;
    N12: TMenuItem;
    N16: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    procedure N6Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure Image1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure N14Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure N16Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure N17Click(Sender: TObject);
  private
    { Private declarations }
  public
    A1, A2, B1, B2, B0: TMyPoint;
    ACoords, BCoords: TCoords;
    n, M: Integer;
    ExA, ExB: Boolean;
    B1B0Length: Real;
    ColorPoint, ColorLine, ColorAxis, ColorLength, ColorDelta, ColorBind,
      ColorBackgr: TColor;
    ProgCfg: TIniFile;
    procedure SetPoint(X, Y: Integer; Name: String);
    procedure ConnectPoints(x1, y1, x2, y2: Integer);
    procedure ClearAll;
    procedure Calculate;
    { Public declarations }
  end;

var
  FormMain: TFormMain;

implementation

uses uPoint, uAbout, uColor;

{$R *.dfm}

procedure TFormMain.SetPoint(X, Y: Integer; Name: String);
begin
with Image1.Canvas do
  begin
  Pen.Width:=2;
  Pen.Color:=ColorPoint;
  Ellipse(X-3,Y-3,X+3,Y+3);
  Pen.Width:=1;
  Pen.Color:=ColorBind;
  MoveTo(X,0);
  LineTo(X,Image1.Height);
  TextOut(X-20,Y-20,Name);
  end;
end;

procedure TFormMain.ConnectPoints(x1, y1, x2, y2: Integer);
begin
with Image1.Canvas do
  begin
  Pen.Width:=2;
  Pen.Color:=ColorLine;
  MoveTo(x1,y1);
  LineTo(x2,y2);
  end;
end;

procedure TFormMain.ClearAll;
begin
with Image1.Canvas do
  begin
  ExA:=False;
  ExB:=False;
  A1[1]:=Image1.Width;
  A2[1]:=Image1.Width;
  B1[1]:=Image1.Width;
  B2[1]:=Image1.Width;
  B0[1]:=Image1.Width;
  A1[2]:=Round(Image1.Height/2);
  A2[2]:=Round(Image1.Height/2);
  B1[2]:=Round(Image1.Height/2);
  B2[2]:=Round(Image1.Height/2);
  B0[2]:=Round(Image1.Height/2);
  ACoords[1]:=0;
  ACoords[2]:=0;
  ACoords[3]:=0;
  BCoords[1]:=0;
  BCoords[2]:=0;
  BCoords[3]:=0;
  FormMain.StatusBar1.Panels[2].Text:='�� ��������';
  FormMain.StatusBar1.Panels[4].Text:='�� ��������';
  FormMain.StatusBar1.Panels[6].Text:='�� ��������';
  n:=1;
  Pen.Width:=1;
  Pen.Color:=clBlack;
  Brush.Color:=ColorBackgr;
  Rectangle(0,0,Image1.Width,Image1.Height);
  Pen.Width:=3;
  Pen.Color:=ColorAxis;
  TextOut(10,Image1.Height div 2-20,'X');
  MoveTo(0,Image1.Height div 2);
  LineTo(Image1.Width,Image1.Height div 2);
  end;
end;

procedure TFormMain.Calculate;
var A1B1, B1B0, CosA, SinA: Real;
begin
if B2[2]<A2[2] then
  begin
  with Image1.Canvas do
    begin
    Pen.Width:=1;
    Pen.Color:=ColorBind;
    MoveTo(0,A2[2]);
    LineTo(Image1.Width,A2[2]);

    Pen.Width:=3;
    Pen.Color:=ColorDelta;
    MoveTo(B2[1],B2[2]);
    LineTo(B2[1],A2[2]);
    TextOut(B2[1]+10,Round((B2[2]+A2[2])/2),'Delta Z');

    Pen.Width:=2;
    Pen.Color:=ColorPoint;
    A1B1:=Sqrt((A1[1]-B1[1])*(A1[1]-B1[1])+(A1[2]-B1[2])*(A1[2]-B1[2]));
    CosA:=(B1[1]-A1[1])/A1B1;
    if A1[2]>B1[2]
      then SinA:=Sqrt(1-CosA*CosA)
      else SinA:=-Sqrt(1-CosA*CosA);
    B1B0:=Abs(A2[2]-B2[2]);
    B0[1]:=Round(B1B0*SinA+B1[1]);
    B0[2]:=Round(B1B0*CosA+B1[2]);
    MoveTo(B1[1],B1[2]);
    LineTo(B0[1],B0[2]);
    Ellipse(B0[1]-3,B0[2]-3,B0[1]+3,B0[2]+3);
    TextOut(B0[1]+10,B0[2]-5,'B0');

    Pen.Width:=3;
    MoveTo(A1[1],A1[2]);
    LineTo(B0[1],B0[2]);
    TextOut(A1[1]+Round({Abs}(B0[1]-A1[1])/2)-15,A1[2]+Round({Abs}(B0[2]-A1[2])/2)-10,'�.�.');

    B1B0Length:=Sqrt((A1[1]-B0[1])*(A1[1]-B0[1])+(A1[2]-B0[2])*(A1[2]-B0[2]));
    StatusBar1.Panels[6].Text:=FloattoStr(Round(B1B0Length*1000)/1000);
    end
  end else
  begin
  with Image1.Canvas do
    begin
    Pen.Width:=1;
    Pen.Color:=ColorBind;
    MoveTo(0,B2[2]);
    LineTo(Image1.Width,B2[2]);

    Pen.Width:=3;
    Pen.Color:=ColorDelta;
    MoveTo(A2[1],A2[2]);
    LineTo(A2[1],B2[2]);
    TextOut(A2[1]-55,Round((B2[2]+A2[2])/2),'Delta Z');

    Pen.Width:=2;
    Pen.Color:=ColorPoint;
    A1B1:=Sqrt((A1[1]-B1[1])*(A1[1]-B1[1])+(A1[2]-B1[2])*(A1[2]-B1[2]));
    CosA:=(B1[1]-A1[1])/A1B1;
    if A1[2]>B1[2]
      then SinA:=Sqrt(1-CosA*CosA)
      else SinA:=-Sqrt(1-CosA*CosA);
    B1B0:=Abs(A2[2]-B2[2]);
    B0[1]:=Round(B1B0*SinA+A1[1]);
    B0[2]:=Round(B1B0*CosA+A1[2]);
    MoveTo(A1[1],A1[2]);
    LineTo(B0[1],B0[2]);
    Ellipse(B0[1]-3,B0[2]-3,B0[1]+3,B0[2]+3);
    TextOut(B0[1]+10,B0[2]+5,'B0');

    Pen.Width:=3;
    MoveTo(B1[1],B1[2]);
    LineTo(B0[1],B0[2]);
    TextOut(B1[1]+Round({Abs}(B0[1]-B1[1])/2)-15,B1[2]+Round({Abs}(B0[2]-B1[2])/2)-10,'�.�.');

    B1B0Length:=Sqrt((B1[1]-B0[1])*(B1[1]-B0[1])+(B1[2]-B0[2])*(B1[2]-B0[2]));
    StatusBar1.Panels[6].Text:=FloattoStr(Round(B1B0Length*1000)/1000);
    end;
  end;
end;

procedure TFormMain.Image1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
label ProcBegin;
begin
ProcBegin:
case n of
  1:begin
    if ExA then
      begin
      Inc(n);
      GoTo ProcBegin;
      end;
    A2[1]:=X;
    A2[2]:=Y;
    SetPoint(X,Y,'A2');
    if ExB then ConnectPoints(A2[1],A2[2],B2[1],B2[2]);
    end;
  2:begin
    if ExB then
      begin
      Inc(n);
      GoTo ProcBegin;
      end;
    B2[1]:=X;
    B2[2]:=Y;
    SetPoint(X,Y,'B2');
    ConnectPoints(A2[1],A2[2],B2[1],B2[2]);
    end;
  3:begin
    if ExA then
      begin
      Inc(n);
      GoTo ProcBegin;
      end;
    X:=A2[1];
    A1[1]:=A2[1];
    A1[2]:=Y;
    SetPoint(X,Y,'A1');
    ExA:=True;
    ACoords[1]:=Image1.Width-A2[1];
    ACoords[2]:=A1[2]-Round(Image1.Height/2);
    ACoords[3]:=Round(Image1.Height/2)-A2[2];
    FormMain.StatusBar1.Panels[2].Text:='('+IntToStr(ACoords[1])+','+IntToStr(ACoords[2])+','+IntToStr(ACoords[3])+')';
    if ExB then ConnectPoints(A1[1],A1[2],B1[1],B1[2]);
    end;
  4:begin
    if ExB then
      begin
      Inc(n);
      GoTo ProcBegin;
      end;
    X:=B2[1];
    B1[1]:=B2[1];
    B1[2]:=Y;
    SetPoint(X,Y,'B1');
    ExB:=True;
    BCoords[1]:=Image1.Width-B2[1];
    BCoords[2]:=B1[2]-Round(Image1.Height/2);
    BCoords[3]:=Round(Image1.Height/2)-B2[2];
    FormMain.StatusBar1.Panels[4].Text:='('+IntToStr(BCoords[1])+','+IntToStr(BCoords[2])+','+IntToStr(BCoords[3])+')';
    ConnectPoints(A1[1],A1[2],B1[1],B1[2]);
    end;
  else Exit;
  end;
Inc(n);
end;

procedure TFormMain.FormCreate(Sender: TObject);
begin
ProgCfg:=TIniFile.Create(ExtractFilePath(ParamStr(0))+'Painter.ini');
Left:=ProgCfg.ReadInteger('form','left',280);
Top:=ProgCfg.ReadInteger('form','top',150);

ColorPoint:=ProgCfg.ReadInteger('colors','point',clRed);
ColorLine:=ProgCfg.ReadInteger('colors','line',clBlack);
ColorAxis:=ProgCfg.ReadInteger('colors','axis',clBlue);
ColorLength:=ProgCfg.ReadInteger('colors','length',clRed);
ColorDelta:=ProgCfg.ReadInteger('colors','delta',clRed);
ColorBind:=ProgCfg.ReadInteger('colors','connection',clTeal);
ColorBackgr:=ProgCfg.ReadInteger('colors','background',clWhite);

Image1.Canvas.Font:=FontDialog1.Font;
N9Click(N9);
end;

procedure TFormMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
ProgCfg.WriteInteger('form','left',Left);
ProgCfg.WriteInteger('form','top',Top);

ProgCfg.WriteInteger('colors','point',ColorPoint);
ProgCfg.WriteInteger('colors','line',ColorLine);
ProgCfg.WriteInteger('colors','axis',ColorAxis);
ProgCfg.WriteInteger('colors','length',ColorLength);
ProgCfg.WriteInteger('colors','delta',ColorDelta);
ProgCfg.WriteInteger('colors','connection',ColorBind);
ProgCfg.WriteInteger('colors','background',ColorBackgr);
end;

procedure TFormMain.FormKeyPress(Sender: TObject; var Key: Char);
begin
case Key of
  #32: N10Click(N10);
  #8, #27: N9Click(N9);
  #49: N6Click(N6);
  #50: N7Click(N7);
  end;
end;

//Menu File
procedure TFormMain.N9Click(Sender: TObject);                 //New
begin
ClearAll;
end;

procedure TFormMain.N8Click(Sender: TObject);                 //Open...
begin
if OpenDialog1.Execute then
  begin
  ClearAll;
  Image1.Picture.LoadFromFile(OpenDialog1.FileName);
  end;
end;

procedure TFormMain.N14Click(Sender: TObject);                //Save As...
begin
if SaveDialog1.Execute then
  begin
  Image1.Picture.SaveToFile(SaveDialog1.FileName);
  end;
end;

procedure TFormMain.N4Click(Sender: TObject);                 //Exit
begin
Close;
end;

//Menu Program
procedure TFormMain.N12Click(Sender: TObject);                //Font...
begin
if FontDialog1.Execute then
  begin
  Image1.Canvas.Font:=FontDialog1.Font;
  end;
end;

procedure TFormMain.N16Click(Sender: TObject);                //Color...
begin
FormColor.ShowModal;
end;

//Menu Functions
procedure TFormMain.N10Click(Sender: TObject);               //Claculate
begin
if ExA and ExB then
  Calculate
  else
    Application.MessageBox(PChar('�� ��� ����� �������'),PChar(FormMain.Caption),mb_ok+mb_iconError)
end;

procedure TFormMain.N6Click(Sender: TObject);                //Point A
begin
if not ExA then
  begin
  FormPoint.Caption:='����� �';
  FormPoint.ShowModal;
  end else
  begin
  Application.MessageBox(PChar('����� � ��� ����������'),PChar(FormMain.Caption),mb_ok+mb_iconError)
  end;
end;

procedure TFormMain.N7Click(Sender: TObject);                 //Point B
begin
if not ExB then
  begin
  FormPoint.Caption:='����� �';
  FormPoint.ShowModal;
  end else
  begin
  Application.MessageBox(PChar('����� � ��� ����������'),PChar(FormMain.Caption),mb_ok+mb_iconError)
  end;
end;

//Menu Help
procedure TFormMain.N3Click(Sender: TObject);                //About..
begin
FormAbout.ShowModal;
end;

procedure TFormMain.N17Click(Sender: TObject);                //HotKeys
begin
Application.MessageBox(PChar(
   'Enter, Space   - ���������'+#10#13
  +'F5, 1     - ����� �'+#10#13
  +'F6, 2     - ����� �'+#10#13#10#13
  +'Ctrl+N, Backspace   - �������'+#10#13
  +'Ctrl+O    - ������� �����������'+#10#13
  +'Ctrl+S    - ��������� �����������'+#10#13#10#13
  +'Ctrl+Q    - ����� �� ���������'+#10#13),
  PChar('������� �������'),MB_IconInformation+MB_OK);
end;

end.
