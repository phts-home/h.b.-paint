object FormPoint: TFormPoint
  Left = 305
  Top = 283
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #1058#1086#1095#1082#1072' '#1040
  ClientHeight = 144
  ClientWidth = 180
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 15
    Top = 20
    Width = 10
    Height = 13
    Caption = 'X:'
  end
  object Label2: TLabel
    Left = 15
    Top = 45
    Width = 10
    Height = 13
    Caption = 'Y:'
  end
  object Label3: TLabel
    Left = 15
    Top = 70
    Width = 10
    Height = 13
    Caption = 'Z:'
  end
  object Edit1: TEdit
    Left = 40
    Top = 20
    Width = 121
    Height = 19
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 0
    OnKeyPress = Edit1KeyPress
  end
  object Edit2: TEdit
    Left = 40
    Top = 45
    Width = 121
    Height = 19
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 1
    OnKeyPress = Edit1KeyPress
  end
  object FlatButton1: TFlatButton
    Left = 90
    Top = 105
    Width = 75
    Height = 25
    BevelOuter = bvNone
    Caption = #1054#1090#1084#1077#1085#1072
    Color = 7851263
    Ctl3D = True
    ParentCtl3D = False
    TabOrder = 2
    OnClick = FlatButton1Click
    ColorNormalState = 7851263
    ButtonType = Flat
    ColorHighLightEnable = True
  end
  object FlatButton2: TFlatButton
    Left = 10
    Top = 105
    Width = 75
    Height = 25
    BevelOuter = bvNone
    Caption = 'Ok'
    Color = 7851263
    Ctl3D = True
    ParentCtl3D = False
    TabOrder = 3
    OnClick = FlatButton2Click
    ColorNormalState = 7851263
    ButtonType = Flat
    ColorHighLightEnable = True
  end
  object Edit3: TEdit
    Left = 40
    Top = 70
    Width = 121
    Height = 19
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 4
    OnKeyPress = Edit1KeyPress
  end
end
