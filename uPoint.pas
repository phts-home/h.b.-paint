unit uPoint;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, FlatButton, StdCtrls;

type
  TFormPoint = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    FlatButton1: TFlatButton;
    FlatButton2: TFlatButton;
    Label3: TLabel;
    Edit3: TEdit;
    procedure FlatButton1Click(Sender: TObject);
    procedure FlatButton2Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormPoint: TFormPoint;

implementation

uses uMain;

{$R *.dfm}

procedure TFormPoint.FlatButton1Click(Sender: TObject);
begin
Close;
end;

procedure TFormPoint.FlatButton2Click(Sender: TObject);
var X, Y1, Y2: Integer;
begin
if Caption='����� �' then
  begin
  X:=FormMain.Image1.Width-StrToInt(Edit1.Text);
  Y1:=Round(FormMain.Image1.Height/2)+StrToInt(Edit2.Text);
  Y2:=Round(FormMain.Image1.Height/2)-StrToInt(Edit3.Text);
  FormMain.A1[1]:=X;
  FormMain.A1[2]:=Y1;
  FormMain.A2[1]:=X;
  FormMain.A2[2]:=Y2;
  FormMain.SetPoint(X,Y2,'A2');
  FormMain.SetPoint(X,Y1,'A1');
  FormMain.ExA:=True;
  FormMain.ACoords[1]:=StrToInt(Edit1.Text);
  FormMain.ACoords[2]:=StrToInt(Edit2.Text);
  FormMain.ACoords[3]:=StrToInt(Edit3.Text);
  FormMain.StatusBar1.Panels[2].Text:='('+Edit1.Text+','+Edit2.Text+','+Edit3.Text+')';
  if FormMain.ExB then
    begin
    FormMain.ConnectPoints(FormMain.A2[1],FormMain.A2[2],FormMain.B2[1],FormMain.B2[2]);
    FormMain.ConnectPoints(FormMain.A1[1],FormMain.A1[2],FormMain.B1[1],FormMain.B1[2]);
    end;
  end else
  begin
  X:=FormMain.Image1.Width-StrToInt(Edit1.Text);
  Y1:=Round(FormMain.Image1.Height/2)+StrToInt(Edit2.Text);
  Y2:=Round(FormMain.Image1.Height/2)-StrToInt(Edit3.Text);
  FormMain.B1[1]:=X;
  FormMain.B1[2]:=Y1;
  FormMain.B2[1]:=X;
  FormMain.B2[2]:=Y2;
  FormMain.SetPoint(X,Y2,'B2');
  FormMain.SetPoint(X,Y1,'B1');
  FormMain.ExB:=True;
  FormMain.BCoords[1]:=StrToInt(Edit1.Text);
  FormMain.BCoords[2]:=StrToInt(Edit2.Text);
  FormMain.BCoords[3]:=StrToInt(Edit3.Text);
  FormMain.StatusBar1.Panels[4].Text:='('+Edit1.Text+','+Edit2.Text+','+Edit3.Text+')';
  if FormMain.ExA then
    begin
    FormMain.ConnectPoints(FormMain.A2[1],FormMain.A2[2],FormMain.B2[1],FormMain.B2[2]);
    FormMain.ConnectPoints(FormMain.A1[1],FormMain.A1[2],FormMain.B1[1],FormMain.B1[2]);
    end;
  end;
Close;
end;

procedure TFormPoint.FormKeyPress(Sender: TObject; var Key: Char);
begin
case Key of
  #13: FlatButton2Click(FlatButton2);
  #27: Close;
  end;
end;

procedure TFormPoint.FormShow(Sender: TObject);
begin
FormPoint.Left:=FormMain.Left+65;
FormPoint.Top:=FormMain.Top+60;
if Caption='����� �' then
  begin
  Edit1.Text:=IntToStr(FormMain.Image1.Width-FormMain.A2[1]);
  Edit2.Text:=IntToStr(FormMain.A1[2]-Round(FormMain.Image1.Height/2));
  Edit3.Text:=IntToStr(Round(FormMain.Image1.Height/2)-FormMain.A2[2]);
  if (Edit1.Text<>'0')and(Edit3.Text<>'0')then
    begin
    Edit1.Enabled:=False;
    Edit3.Enabled:=False;
    end else
    begin
    Edit1.Enabled:=True;
    Edit3.Enabled:=True;
    end;
  end else
  begin
  Edit1.Text:=IntToStr(FormMain.Image1.Width-FormMain.B2[1]);
  Edit2.Text:=IntToStr(FormMain.B1[2]-Round(FormMain.Image1.Height/2));
  Edit3.Text:=IntToStr(Round(FormMain.Image1.Height/2)-FormMain.B2[2]);
  if (Edit1.Text<>'0')and(Edit3.Text<>'0')then
    begin
    Edit1.Enabled:=False;
    Edit3.Enabled:=False;
    end else
    begin
    Edit1.Enabled:=True;
    Edit3.Enabled:=True;
    end;
  end;
end;

procedure TFormPoint.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
if (Key<#48)or(Key>#57) then Key:=#0;
end;

end.
